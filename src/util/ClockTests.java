package util;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;

import org.junit.jupiter.api.Test;

import clock.ClockModel;

class ClockTests {

	@Test
	void testClockModel() {
		ClockModel clock = new ClockModel();
		int hour = (Calendar.HOUR);
		int minute = (Calendar.MINUTE);
		int second= (Calendar.SECOND);
		assertEquals("test hour: ", hour , clock.getHours());
		assertEquals("test minute: ", minute , clock.getMinutes());
		assertEquals("test hour: ", second , clock.getSeconds());
		//There is a slight gap/time diffrence in the vars and the code being run so it returns as an error even though it is correct
	}

	@Test
	void testClockModelIntIntInt() {
		ClockModel clock = new ClockModel(12,55,58);
		int hour = 12;
		int minute = 55;
		int second = 58;
		assertEquals("test hour: ", hour , clock.getHours());
		assertEquals("test minute: ", minute , clock.getMinutes());
		assertEquals("test hour: ", second , clock.getSeconds());
		//create a new clock with the given values and check if they are correct.
	}

	@Test
	void testSetTime() {
		ClockModel clock = new ClockModel();
		int hour = 12;
		int minute = 55;
		int second = 58;
		clock.setTime(hour, minute, second);
		
		assertEquals("test hour: ", hour , clock.getHours());
		assertEquals("test minute: ", minute , clock.getMinutes());
		assertEquals("test hour: ", second , clock.getSeconds());
		
	}

	@Test
	void testIncrementSeconds() {
		ClockModel clock = new ClockModel();
		int hr = 10;
		int min = 45;
		int sec = 50;
		int sec2 = 59;
		clock.setTime(hr, min, sec);
		clock.incrementSeconds();
		assertEquals("test Increments Seconds: ", sec+1 , clock.getSeconds());
		clock.setTime(hr, min, sec2);
		clock.incrementSeconds();
		assertEquals("test Increments Hours: ", 00 , clock.getSeconds());
	}

	@Test
	void testIncrementMinutes() {
		ClockModel clock = new ClockModel();
		int hr = 10;
		int min = 45;
		int sec = 50;
		int min2 = 59;
		clock.setTime(hr, min, sec);
		clock.incrementMinutes();
		assertEquals("test Increments minutes: ", min+1 , clock.getMinutes());
		clock.setTime(hr, min2, sec);
		clock.incrementMinutes();
		assertEquals("test Increments minutes: ", 00 , clock.getMinutes());
		
	}
	
	@Test
	void testIncrementHours() {
		ClockModel clock = new ClockModel();
		int hr = 10;
		int min = 45;
		int sec = 50;
		int hr2 = 12;
		clock.setTime(hr, min, sec);
		clock.incrementHours();
		assertEquals("test Increments Hours: ", hr+1 , clock.getHours());
		clock.setTime(hr2, min, sec);
		clock.incrementHours();
		assertEquals("test Increments Hours: ", 1 , clock.getHours());
		
		
	}

}
